# Related Papers
- Identity Mappings in Deep Residual Networks (https://arxiv.org/pdf/1603.05027v2.pdf)
- Deep Residual Learning for Image Recognition (https://arxiv.org/pdf/1512.03385v1.pdf)

# Original Github Repo
- https://github.com/KaimingHe/deep-residual-networks